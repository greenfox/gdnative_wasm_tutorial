FROM ubuntu:20.04

#small container usablity apps
RUN apt-get update && apt-get install -y \
    zip \
    wget \
    curl \
    unzip \
    nano \
    p7zip-full \
    && rm -rf /var/lib/apt/lists/*

#apps required to build Godot-Rust on Windows, Linux, and WASM.
RUN apt-get update && apt-get install -y \
    git \
    curl \
    mingw-w64 \
    libclang-dev \
    clang \
    && rm -rf /var/lib/apt/lists/*

#install rust
RUN curl https://sh.rustup.rs -sSf | sh -s -- --default-toolchain stable -y

#set up rustup nightly for emscripten (idk why the PATH is fucked?)
RUN /root/.cargo/bin/rustup update \
    && /root/.cargo/bin/rustup update nightly \
    && /root/.cargo/bin/rustup target install wasm32-unknown-emscripten --toolchain nightly

#Godot 3.5 exports use Emscripten 3.1.10
#Godot 3.5.1 exports use Emscripten 3.1.14
ARG EMSCRIPTEN_VERSION=3.1.14

#install emscripten 
RUN git clone http://github.com/emscripten-core/emsdk.git \
    && cd emsdk \
    && ./emsdk install ${EMSCRIPTEN_VERSION} \
    && ./emsdk activate ${EMSCRIPTEN_VERSION} \
    && rm -rf .git \
    && echo 'source "/emsdk/emsdk_env.sh"' >> $HOME/.bashrc

#env path needed for rust-emscripten
ENV C_INCLUDE_PATH=/emsdk/upstream/emscripten/cache/sysroot/include
ENV PATH="/emsdk:/emsdk/upstream/emscripten:/emsdk/node/14.18.2_64bit/bin:/root/.cargo/bin:${PATH}"