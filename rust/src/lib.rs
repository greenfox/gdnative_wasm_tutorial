use gdnative::prelude::*;

fn init(handle: InitHandle) {
    handle.add_class::<RustTest>();
}

godot_init!(init);

/// The HelloWorld "class"
#[derive(NativeClass)]
#[inherit(Label)]
pub struct RustTest;

#[methods]
impl RustTest {
    fn new(_base: &Label) -> Self {
        godot_print!("Hello from Rust!");
        RustTest
    }
    #[method]
    fn _ready(&self, #[base] base: &Label) {
        let build_target = gdnative::api::OS::godot_singleton().get_name();
        base.set_text(GodotString::from("This label was changed from Rust!\nThe build target that changed it was:") + build_target);
    }
}
